@extends('layouts.app')

@section('content')
<div class='content-container'>
	<div class='standard-show'>
		<h2>{{ $server->name }} ({{ $server->servertype->type }})</h2>
		<h4 class='description'>{{ $server->description }}</h4>
		<ul class='details'>
			<li><strong>Address: </strong>{{ $server->address }} ({{ $server->port }})</li>
			<li><strong>Username: </strong>{{ $server->username }}</li>
			<li><strong>Password: </strong>************</li>
			<li><strong>SSH Username: </strong>{{ $server->ssh_username }}</li>
			<li><strong>SSH Password: </strong>************</li>
			
		</ul>
		<a class="button" href="{{ route('servers.index') }}">
			Back to Servers
		</a>
	</div>
</div>
    
@endsection
