@extends('layouts.app')

@push('scripts')
<script type="text/javascript">
	var form;

	window.onload = function () {
		form = new Vue({
		    el: '.content-container form',
		    data: {
		    	server_type_id:  {{ (old('server_type_id')) ? old('server_type_id') : 0 }},
		    	ports: {!! $server_types->keyBy('id')
		    							->map(function ($server_type) { 
		    								return $server_type['default_port'];
		    							})->toJSON() !!},
		    	port: {{ (old('port')) ? old('port') : 0 }}
		    },
		    watch: {
		    	server_type_id: function () {
		    		if (port = this.ports[this.server_type_id]) {
		    			this.port = port;
		    		}
		    	}
		    }
		});	
	}
</script>
@endpush

@section('content')
<div class='content-container'>
	<h1>Create Server</h1>
	<form method="POST" action="/servers">
		{{ csrf_field() }}

		<select  v-model="server_type_id" name='server_type_id'>
			<option value=0 disabled>Server Type</option>
			@foreach($server_types as $server_type)
				<option value="{{ $server_type->id }}">{{ $server_type->type }}</option>
			@endforeach
		</select>


		<div class="form-group">
			<input class="{{ $errors->has('name') ? ' has-error' : '' }}" type="text" placeholder="Name" name="name" value="{{ old('name') }}" required autofocus>

			@if ($errors->has('name'))
				<span class="help-block">
					<strong>{{ $errors->first('name') }}</strong>
				</span>
			@endif
		</div>

		<div class="form-group">
			<textarea class="{{ $errors->has('description') ? ' has-error' : '' }}" type="text" placeholder="Description" name="description" >{{ old('description') }}</textarea>

			@if ($errors->has('description'))
				<span class="help-block">
					<strong>{{ $errors->first('description') }}</strong>
				</span>
			@endif
		</div>

		<div class="form-group">
			<input class="{{ $errors->has('address') ? ' has-error' : '' }}" type="text" placeholder="Address" name="address" value="{{ old('address') }}">

			@if ($errors->has('address'))
				<span class="help-block">
					<strong>{{ $errors->first('address') }}</strong>
				</span>
			@endif
		</div>

		<div class="form-group">
			<input class="{{ $errors->has('port') ? ' has-error' : '' }}" type="number" v-model="port" placeholder="Port" name="port" >

			@if ($errors->has('port'))
				<span class="help-block">
					<strong>{{ $errors->first('port') }}</strong>
				</span>
			@endif
		</div>

		<div class="form-group">
			<input class="{{ $errors->has('username') ? ' has-error' : '' }}" type="text" placeholder="Username" name="username" value="{{ old('username') }}">

			@if ($errors->has('username'))
				<span class="help-block">
					<strong>{{ $errors->first('username') }}</strong>
				</span>
			@endif
		</div>

		<div class="form-group">
			<input class="{{ $errors->has('password') ? ' has-error' : '' }}" type="password" placeholder="Password" name="password">

			@if ($errors->has('password'))
				<span class="help-block">
					<strong>{{ $errors->first('password') }}</strong>
				</span>
			@endif
		</div>

		<div class="form-group">
			<input class="{{ $errors->has('ssh_username') ? ' has-error' : '' }}" type="text" placeholder="SSH Username" name="ssh_username" value="{{ old('ssh_username') }}">

			@if ($errors->has('ssh_username'))
				<span class="help-block">
					<strong>{{ $errors->first('ssh_username') }}</strong>
				</span>
			@endif
		</div>

		<div class="form-group">
			<input class="{{ $errors->has('ssh_password') ? ' has-error' : '' }}" type="password" placeholder="SSH Password" name="ssh_password" >

			@if ($errors->has('ssh_password'))
				<span class="help-block">
					<strong>{{ $errors->first('ssh_password') }}</strong>
				</span>
			@endif
		</div>

		<div class="form-group">
			<button type="submit">
				Create
			</button>

			<a class="button" href="\servers">
				Back
			</a>
		</div>
	</form>
</div>
    
@endsection
