<div class='standard-list-row'>
	<span>
		{{ $server->name }} ({{ $server->serverType->type }})
	</span>
	<span>{{ $server->address }}</span>
	<span>{{ $server->port }}</span>
	<span>
		<a class='button button-row' href="{{ route('servers.edit', ['id' => $server->id]) }}">Edit</a>
		<form  class='delete-form' method="POST" action="{{ route('servers.destroy', ['id' => $server->id]) }}">
			{{ csrf_field() }}
			{{ method_field('DELETE') }}
			<button class='button-row'">Delete</button>
		</form>
	</span>
</div>