@extends('layouts.app')

@section('content')
<div class='content-container'>
	<div class='content-header'>
		<a class='button' href="servers/create">Add Server</a>
	</div>
	
	<div class='standard-list'>
		@if($servers->count())
			<div class='standard-list-row standard-list-header'>
				<span>Name</span>
				<span>Address</span>
				<span>Port</span>
				<span>Action</span>
			</div>
		@endif
		@each('servers.index.row', $servers, 'server', 'servers.index.empty')
	</div>
</div>
    
@endsection
