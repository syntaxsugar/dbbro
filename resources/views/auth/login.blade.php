@extends('layouts.app')

@section('content')

<div class='auth-form-containter'>
	<form method="POST" action="{{ route('login') }}">
		{{ csrf_field() }}

		<div class="form-group">
			<input id="email" class="{{ $errors->has('email') ? ' has-error' : '' }}" type="email" placeholder="E-Mail Address" name="email" value="{{ old('email') }}" required autofocus>

			@if ($errors->has('email'))
				<span class="help-block">
					<strong>{{ $errors->first('email') }}</strong>
				</span>
			@endif
		</div>

		<div class="form-group">
			<input id="password" class="{{ $errors->has('password') ? ' has-error' : '' }}"type="password" placeholder="Password" name="password" required>

			@if ($errors->has('password'))
				<span class="help-block">
					<strong>{{ $errors->first('password') }}</strong>
				</span>
			@endif
		</div>

		<div class="form-group">
			<div class='checkbox'>
				<input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
			</div>
		</div>

		<div class="form-group">
			<button type="submit">
				Login
			</button>

			<a class="button" href="{{ route('password.request') }}">
				Forgot Your Password?
			</a>
			
		</div>
	</form>
</div>

@endsection
