@extends('layouts.app')

@section('content')
<div class='auth-form-containter'>
	<form method="POST" action="{{ route('register') }}">
		{{ csrf_field() }}

		<div class="form-group">
			<input id="name" type="text" placeholder='Name' class="{{ $errors->has('name') ? ' has-error' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

			@if ($errors->has('name'))
				<span class="help-block">
					<strong>{{ $errors->first('name') }}</strong>
				</span>
			@endif
		</div>

		<div class="form-group">
			<input id="email" placeholder="E-Mail Address" type="email" class="{{ $errors->has('email') ? ' has-error' : '' }}" name="email" value="{{ old('email') }}" required>

			@if ($errors->has('email'))
				<span class="help-block">
					<strong>{{ $errors->first('email') }}</strong>
				</span>
			@endif
		</div>

		<div class="form-group">
			<input id="password" placeholder="Password" type="password" class="{{ $errors->has('password') ? ' has-error' : '' }}" name="password" required>

			@if ($errors->has('password'))
				<span class="help-block">
					<strong>{{ $errors->first('password') }}</strong>
				</span>
			@endif
		</div>

		<div class="form-group">
			<input id="password-confirm" placeholder="Confirm Password" type="password" name="password_confirmation" required>
		</div>

		<div class="form-group">
			<button type="submit" class="button">
				Register
			</button>
		</div>
	</form>

</div>
@endsection
