@extends('layouts.app')

@section('content')
<div class='content-container'>
	<div class='standard-show'>
		<h2>{{ $backup->database->name }}</h2>
		<h4 class='description'>{{ $backup->database->server->name }} ({{ $backup->database->server->serverType->type }})</h4>
		<ul class='details'>
			<li><strong>Creation Date: </strong>{{ $backup->created_at }}</li>
		</ul>
		<a class="button" href="{{ route('backups.index') }}">
			Back to Backups
		</a>
	</div>
</div>
    
@endsection
