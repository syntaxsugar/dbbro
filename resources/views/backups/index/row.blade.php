<div class='standard-list-row'>
	<span>{{ $backup->database->name }}</span>
	<span>{{ $backup->database->server->name }} ({{ $backup->database->server->serverType->type }})</span>
	<span>{{ $backup->created_at }}</span>
	<span>
		<a class='button button-row' href="{{ route('backups.show', ['id' => $backup->id]) }}">Show</a>
		<form  class='delete-form' method="POST" action="{{ route('backups.destroy', ['id' => $backup->id]) }}">
			{{ csrf_field() }}
			{{ method_field('DELETE') }}
			<button class='button-row'">Delete</button>
		</form>
	</span>
</div>