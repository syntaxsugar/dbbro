@extends('layouts.app')

@section('content')
<div class='content-container'>
	<div class='standard-list'>
		@if($backups->count())
			<div class='standard-list-row standard-list-header'>
				<span>Database</span>
				<span>Server</span>
				<span>Creation Date</span>
				<span>Action</span>
			</div>
		@endif
		@each('backups.index.row', $backups, 'backup', 'backups.index.empty')
	</div>
</div>
    
@endsection
