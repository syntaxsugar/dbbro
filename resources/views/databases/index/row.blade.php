<div class='standard-list-row'>
	<span>
		{{ $database->name }} 
	</span>
	<span>{{ $database->description }}</span>
	<span>{{ $database->server->name }} ({{ $database->server->serverType->type }})</span>
	<span>
		<a class='button button-row' href="{{ route('databases.edit', ['id' => $database->id]) }}">Edit</a>
		<a class='button button-row' href="{{ route('databases.backup', ['id' => $database->id]) }}">Backup</a>
		<form  class='delete-form' method="POST" action="{{ route('databases.destroy', ['id' => $database->id]) }}">
			{{ csrf_field() }}
			{{ method_field('DELETE') }}
			<button class='button-row'">Delete</button>
		</form>
	</span>
</div>