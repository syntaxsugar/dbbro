@extends('layouts.app')

@section('content')
<div class='content-container'>
	<h1>Create Database</h1>
	<form method="POST" action="{{ route('databases.store') }}">
		{{ csrf_field() }}
		<select   name='server_id'>
			<option value=0 disabled>Server</option>
			@foreach($servers as $server)
				<option value="{{ $server->id }}"  {{ (old('server_id') == $server->id) ? 'selected' : '' }} >{{ $server->name }} ({{ $server->serverType->type }})</option>
			@endforeach
		</select>

		<div class="form-group">
			<input class="{{ $errors->has('name') ? ' has-error' : '' }}" type="text" placeholder="Name" name="name" value="{{ old('name') }}" required autofocus>

			@if ($errors->has('name'))
				<span class="help-block">
					<strong>{{ $errors->first('name') }}</strong>
				</span>
			@endif
		</div>

		<div class="form-group">
			<textarea class="{{ $errors->has('description') ? ' has-error' : '' }}" type="text" placeholder="Description" name="description" >{{ old('description') }}</textarea>

			@if ($errors->has('description'))
				<span class="help-block">
					<strong>{{ $errors->first('description') }}</strong>
				</span>
			@endif
		</div>

		<div class="form-group">
			<button type="submit">
				Create
			</button>

			<a class="button" href="{{ route('databases.index') }}">
				Back
			</a>
		</div>
	</form>
</div>
    
@endsection
