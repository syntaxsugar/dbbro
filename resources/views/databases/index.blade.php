@extends('layouts.app')

@section('content')
<div class='content-container'>
	<div class='content-header'>
		<a class='button' href="{{ route('databases.create') }}">Add Database</a>
	</div>
	
	<div class='standard-list'>
		@if($databases->count())
			<div class='standard-list-row standard-list-header'>
				<span>Name</span>
				<span>Description</span>
				<span>Server</span>
				<span>Action</span>
			</div>
		@endif
		@each('databases.index.row', $databases, 'database', 'databases.index.empty')
	</div>
</div>
    
@endsection
