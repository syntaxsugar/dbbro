@extends('layouts.app')
<?php
	$server_id = (old('server_id')) ? old('server_id') : $database->server->id;

?>
@section('content')
<div class='content-container'>
	<h1>Edit Database</h1>
	<form method="POST" action="{{ route('databases.update', ['id' => $database->id]) }}">
		{{ csrf_field() }}
		{{ method_field('PATCH') }}
		<select   name='server_id'>
			<option value=0 disabled>Server</option>
			@foreach($servers as $server)
				<option value="{{ $server->id }}" {{ ($server->id == $server_id) ? 'selected' : '' }} >{{ $server->name }} ({{ $server->serverType->type }})</option>
			@endforeach
		</select>

		<div class="form-group">
			<input class="{{ $errors->has('name') ? ' has-error' : '' }}" type="text" placeholder="Name" name="name" value="{{ (old('name')) ? old('name') : $database->name }}" required autofocus>

			@if ($errors->has('name'))
				<span class="help-block">
					<strong>{{ $errors->first('name') }}</strong>
				</span>
			@endif
		</div>

		<div class="form-group">
			<textarea class="{{ $errors->has('description') ? ' has-error' : '' }}" type="text" placeholder="Description" name="description" >{{ (old('description')) ? old('description') : $database->description }}</textarea>

			@if ($errors->has('description'))
				<span class="help-block">
					<strong>{{ $errors->first('description') }}</strong>
				</span>
			@endif
		</div>

		<div class="form-group">
			<button type="submit">
				Save
			</button>

			<a class="button" href="{{ route('databases.index') }}">
				Back
			</a>
		</div>
	</form>
</div>
    
@endsection
