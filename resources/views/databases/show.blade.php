@extends('layouts.app')

@section('content')
<div class='content-container'>
	<div class='standard-show'>
		<h2>{{ $database->name }}</h2>
		<h4 class='description'>{{ $database->description }}</h4>
		<ul class='details'>
			<li><strong>Server: </strong>{{ $database->server->name }} ({{ $database->server->serverType->type }})</li>
		</ul>
		<a class="button" href="{{ route('databases.index') }}">
			Back to Databases
		</a>
	</div>
</div>
    
@endsection
