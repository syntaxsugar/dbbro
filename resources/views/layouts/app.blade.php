<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'Laravel') }}</title>

	<!-- Styles -->
	<link href="/css/app.css" rel="stylesheet">

	<!-- Scripts -->
	<script>
		window.Laravel = {!! json_encode([
			'csrfToken' => csrf_token(),
		]) !!};    
	</script>
	<script type="text/javascript" src="/js/app.js"></script>
	@stack('scripts')
</head>
<body>
	<div id="app" class="{{ (Auth::check()) ? 'logged-in' : ''}}">
		<nav class="navbar">
			<a class="navbar-brand" href="{{ url('/') }}">
				{{ config('app.name', 'Laravel') }}
			</a>
			<ul class="navbar-right">
				<!-- Authentication Links -->
				@if (Auth::guest())
					<li><a href="{{ route('login') }}">Login</a></li>
					<li><a href="{{ route('register') }}">Register</a></li>
				@else
					<li >
						<a href="#" >
							{{ Auth::user()->name }} 
						</a>
					</li>
				@endif
			</ul>
		</nav>
		@if (Auth::check())
		<div class='app-nav'>
			<router-link to="/schedules" >Schedules</router-link>
			<router-link to="/servers" >Servers</router-link>
			<router-link to="/databases" >Databases</router-link>
			<router-link to="/backups" >Backups</router-link>
			<a href="{{ route('logout') }}"
				onclick="event.preventDefault();
						 document.getElementById('logout-form').submit();">
				Logout
			</a>

			<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
				{{ csrf_field() }}
			</form>
		</div>
		<router-view id='app-content' class='app-content'>
			@yield('content')
		</router-view>
		@else
		<div id='app-content' class='app-content'>@yield('content')</div>
		@endif

		


	</div>

</body>
</html>
