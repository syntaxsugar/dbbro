
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import Vue from 'vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'

Vue.use(VueRouter)
Vue.use(VueResource)

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const SchedulesIndex = resolve => require(['./components/Schedules/index.vue'], resolve)
const SchedulesEdit = resolve => require(['./components/Schedules/edit.vue'], resolve)
const SchedulesShow = resolve => require(['./components/Schedules/show.vue'], resolve)

const ServersIndex = resolve => require(['./components/Servers/index.vue'], resolve)
const ServersEdit = resolve => require(['./components/Servers/edit.vue'], resolve)
const ServersShow = resolve => require(['./components/Servers/show.vue'], resolve)


const DatabasesIndex = resolve => require(['./components/Databases/index.vue'], resolve)
const DatabasesEdit = resolve => require(['./components/Databases/edit.vue'], resolve)
const DatabasesShow = resolve => require(['./components/Databases/show.vue'], resolve)

const BackupsIndex = resolve => require(['./components/Backups/index.vue'], resolve)
const BackupsShow = resolve => require(['./components/Backups/show.vue'], resolve)


window.onload = function () {
	const routes = [
		{ path: '/schedules', component: SchedulesIndex },
		{ path: '/schedules/create', component: SchedulesEdit },
		{ path: '/schedules/:id/edit', component: SchedulesEdit },
		{ path: '/schedules/:id', component: SchedulesShow },

		{ path: '/servers', component: ServersIndex },
		{ path: '/servers/create', component: ServersEdit },
		{ path: '/servers/:id/edit', component: ServersEdit },
		{ path: '/servers/:id', component: ServersShow },

		{ path: '/databases', component: DatabasesIndex },
		{ path: '/databases/create', component: DatabasesEdit },
		{ path: '/databases/:id/edit', component: DatabasesEdit },
		{ path: '/databases/:id', component: DatabasesShow },

		{ path: '/backups', component: BackupsIndex },
		{ path: '/backups/:id', component: BackupsShow },
	]

	const router = new VueRouter({
	  	routes, // short for routes: routes
	  	linkActiveClass: 'selected'
	})

	const app = new Vue({
	  	router,
	  	http: {
	    	root: '/'
	  	}
	}).$mount('#app')

	Vue.http.interceptors.push(function(request, next) {

	  	// modify headers
	  	request.headers.set('X-CSRF-TOKEN', Laravel.csrfToken);

	  	// continue to next interceptor
	  	next();
	});	
}



//router.push('/manage');