<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Backup;
use App\Jobs\CreateBackupFile;

class Schedule extends Model
{
    protected $guarded = [];

    public function user() {
		return $this->belongsTo('App\User');
	}

	public function database() {
		return $this->belongsTo('App\Database');
	}

	public function backups() {
		return $this->hasMany('App\Backup');
	}

	public function scopeAt($query, $time) {
		
		return $query->where('start_date', '<=', $time)
					 ->where(function ($query) use ($time) {

					 	$query->where(function ($query) use ($time) {
				   			$query->whereFrequency('once')
				   				  ->where('time', $time->format('H:i'))
				   				  ->whereStartDate($time->format('Y-m-d'));
				   		});

					 	$query->orwhere(function ($query) use ($time) {
				   			$query->whereFrequency('monthly')
				   				  ->where('time', $time->format('H:i'))
				   				  ->whereRaw('DAY(start_date) = ?', [ $time->format('d') ]);
				   		});

					 	$query->orwhere(function ($query) use ($time) {
				   			$query->whereFrequency('weekly')
				   				  ->where('time', $time->format('H:i'))
				   				  ->whereRaw('DAYOFWEEK(start_date) = ?', [ $time->format('w') + 1 ]);
				   		});
				   		
				   		$query->orWhere(function ($query) use ($time) {
				   			$query->whereFrequency('daily')
				   				  ->where('time', $time->format('H:i'));
				   		});

				   		$query->orWhere(function ($query) use ($time) {
				   			$time_string = $time->format('Y-m-d H:i');
				   			$query->whereFrequency('hourly')
				   				  ->whereRaw("MINUTE('$time_string') = 0");
				   		});
				   });
	}

	public function createBackup() {
		$this->load('database', 'database.server', 'database.server.serverType');

		$backup = new Backup();
		$backup->database()->associate($this->database);

		$backup = $this->backups()->save($backup);

        $job = (new CreateBackupFile($backup))->onQueue('backups');

        dispatch($job);       
	}
}
