<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServerType extends Model
{
    public $timestamps = false;

    public function servers () {
    	return $this->hasMany('App\Server');
    }

    public function databases () {
    	return $this->hasMany('App\Database', 'App\Server');
    }
}
