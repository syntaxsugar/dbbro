<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use SSH;
use Storage;
use File;
use PharData;
use Phar;
use App\Jobs\CreateBackupFile;

class Database extends Model
{
    protected $guarded = [];

    public function user() {
		return $this->belongsTo('App\User');
	}

    public function server() {
		return $this->belongsTo('App\Server');
	}

	public function schedules() {
		return $this->hasMany('App\Schedule');
	}

	public function backups() {
		return $this->hasMany('App\Backup');
	}

	public function createBackup() {
	 	$this->load('server', 'server.serverType');

        $backup = $this->backups()->create([]);

        $job = (new CreateBackupFile($backup))->onQueue('backups');

        dispatch($job);        
	}
}
