<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use PharData;
use Phar;

class CompressBackupFile implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $file;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($file)
    {
        $this->file = $file;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        ini_set('memory_limit','10G');

        $path_info = pathinfo($this->file);
        $phar_file = "$path_info[dirname]/$path_info[basename].tar";
        $phar = new PharData($phar_file);
        $phar->addFile($this->file, "$path_info[basename].sql");
        $phar->compress(Phar::GZ);
        unlink($phar_file);
        unlink($this->file);
    }
}
