<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Database;
use App\Backup;
use Auth;
use Validator;

class BackupController extends Controller
{
    public function index()
    {
    	$backups = Backup::with('database', 'database.server', 'database.server.serverType')
                         ->whereHas('database.server.user', function ($query) {
    		return $query->whereId(Auth::user()->id);
    	})->get();

        return response()->json(compact('backups'));
    }

    public function show($id) {
		$backup = Backup::with('database', 'database.server', 'database.server.serverType')->find($id);

		return response()->json(compact('backup'));
    }

    public function destroy($id) {
    	$backup = Backup::find($id);
        $backup->deleteBackupFile();
    	$backup->delete();
    }
}
