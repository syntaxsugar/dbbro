<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Database;
use App\Jobs\CreateDatabaseBackup;
use Auth;
use Validator;

class DatabaseController extends Controller
{
    public function index()
    {
    	$user = Auth::user();
    	$databases = $user->databases()
    					  ->with('server', 'server.serverType')
    				   	  ->get(); 

        return response()->json(compact('databases'));
    }

    public function create() {
    	$user = Auth::user();
    	$servers = $user->servers()
    					->with('serverType')
    				   	->get(); 

        return response()->json(compact('servers'));
    }

    public function edit($id) {
        $database = Database::with('server', 'server.serverType')->find($id);

        $user = Auth::user();
        $servers = $user->servers()
                        ->with('serverType')
                        ->get(); 

        return response()->json(compact('database', 'servers'));
    }

    public function store(Request $request) {

    	Validator::make($request->all(), [
		    'server_id' => 'required|exists:servers,id',
            'name' 		=> 'required|max:255'
		])->validate();

		$database = new Database([
				'name' => $request->name,
				'description' => $request->description
		]);

		$server = Auth::user()->servers()
							  ->whereId($request->server_id)
							  ->first();

		$database->server()->associate($server);

		$database->save();
    }

    public function update(Request $request, $id) {

    	//dd($request);

    	Validator::make($request->all(), [
		    'server_id' => 'required|exists:servers,id',
            'name' 		=> 'required|max:255'
		])->validate();

		$database = Database::find($id);

		$database->name = $request->name;
		$database->description = $request->description;
		$database->server_id = $request->server_id;
		
		$database->save();
    }

    public function show($id) {
		$database = Database::with('server', 'server.serverType')->find($id);

        return response()->json(compact('database'));
    }

    public function destroy($id) {
    	$database = Database::find($id);
    	$database->delete();
    }

    public function backup($id) {
        $database = Database::find($id);

        $database->createBackup();
    }
}
