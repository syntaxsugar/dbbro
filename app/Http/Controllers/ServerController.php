<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Server;
use App\ServerType;
use Validator;
use Auth;
use Crypt;

class ServerController extends Controller
{
    public function index()
    {

    	$user = Auth::user();
    	$servers = $user->servers()
    					->with('serverType')
    					->get();

        return response()->json(compact('servers'));
    }

    public function create() {
    	$server_types = ServerType::get();
        $ports = $server_types->keyBy('id')
                              ->map(function ($server_type) { 
                                    return $server_type['default_port'];
                                });

        return response()->json(compact('server_types', 'ports'));
    }

    public function edit($id) {
        $server_types = ServerType::get();
        $server = Server::with('serverType')->find($id);
        $ports = $server_types->keyBy('id')
                              ->map(function ($server_type) { 
                                    return $server_type['default_port'];
                                });
        $server = Server::with('serverType')->find($id);

        return response()->json(compact('server', 'server_types', 'ports'));
    }

    public function store(Request $request) {
    	Validator::make($request->all(), [
		    'server_type_id' => 'required|exists:server_types,id',
            'name' => 'required|max:255',
            'address' => 'required|max:255',
            'port' => 'required|numeric|max:65535',
            'username' => 'required|max:255',
            'password' => 'required|max:255',
            'ssh_username' => 'required|max:255',
            'ssh_password' => 'required|max:255'
		])->validate();

		

		$server = Auth::user()->servers()->create([
				'server_type_id' => $request->server_type_id,
				'name' => $request->name,
				'description' => $request->description,
				'address' => $request->address,
				'port' => $request->port,
				'username' => $request->username,
				'password' => Crypt::encrypt($request->password),
				'ssh_username' => $request->ssh_username,
				'ssh_password' => Crypt::encrypt($request->ssh_password),
		]);

		$server->save();
    }

    public function update(Request $request, $id) {

    	Validator::make($request->all(), [
		    'server_type_id' => 'required|exists:server_types,id',
            'name' => 'required|max:255',
            'address' => 'required|max:255',
            'port' => 'required|numeric|max:65535',
            'username' => 'required|max:255',
            'password' => 'max:255',
            'ssh_username' => 'required|max:255',
            'ssh_password' => 'max:255'
		])->validate();

		$server = Server::find($id);

		$server->name = $request->name;
		$server->description = $request->description;
		$server->address = $request->address;
		$server->port = $request->port;
		$server->username = $request->username;

		if ($request->password) {
			$server->password = $request->password;
		}

		$server->ssh_username = $request->ssh_username;
			
		if ($request->ssh_password) {
			$server->ssh_password = $request->ssh_password;
		}

		$server->save();
    }

    public function show($id) {
		$server = Server::with('serverType')->find($id);
        return response()->json(compact('server'));
		
    }

    public function destroy($id) {
    	$server = Server::find($id);
    	$server->delete();
    }
}
