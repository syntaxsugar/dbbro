<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Validator;
use App\Schedule;

class ScheduleController extends Controller
{
    public function index()
    {
    	$user = Auth::user();
    	$schedules = $user->schedules()
    					  ->with('database', 'database.server')
    				   	  ->get(); 

        return response()->json(compact('schedules'));
    }

    public function create() {
    	$user = Auth::user();
    	$databases = $user->databases()
    					  ->with('server')
    				   	  ->get();

        return response()->json(compact('databases'));
    }

    public function edit($id) {
        $schedule = Schedule::with('database', 'database.server')->find($id);

        $user = Auth::user();
        $databases = $user->databases()
                          ->with('server')
                          ->get(); 

        return response()->json(compact('schedule', 'databases'));
    }

    public function store(Request $request) {

    	Validator::make($request->all(), [
		    'database_id' => 'required|exists:databases,id',
		    'start_date' => 'required|date|after:yesterday',
		    'frequency'	=> 'required|in:once,hourly,daily,weekly,monthly'
		])->validate();

		$user = Auth::user();

		$user->schedules()->create($request->all());
    }

    public function update(Request $request, $id) {

    	//dd($request);

    	Validator::make($request->all(), [
		    'database_id' => 'required|exists:databases,id',
		    'start_date' => 'required|date|after:yesterday',
		    'frequency'	=> 'required|in:once,hourly,daily,weekly,monthly'
		])->validate();

		$schedule = Schedule::find($id);

		$schedule->database_id = $request->database_id;
		$schedule->start_date = $request->start_date;
		$schedule->time = $request->time;
		$schedule->frequency = $request->frequency;
		
		$schedule->save();
    }

    public function show($id) {
		$schedule = schedule::with('database', 'database.server')->find($id);

        return response()->json(compact('schedule'));
    }

    public function destroy($id) {
    	$schedule = Schedule::find($id);
    	$schedule->delete();
    }

    public function backup($id) {
        $database = Database::find($id);

        $database->createBackup();
    }
}
