<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Jobs\CompressBackupFile;
use File;
use SSH;

class Backup extends Model {

	protected $guarded = [];

	public function schedule() {
		return $this->belongsTo('App\Schedule');
	}

	public function database() {
		return $this->belongsTo('App\Database');
	}
	public function createBackupFile() {
		$this->load('database','database.server','database.server.serverType');
        $backup_dir = storage_path('app/public/backups/' . date('Y-m-d')); 

        //Create directory if it doesn't exist
        if (!File::isDirectory($backup_dir)) {
        	File::makeDirectory($backup_dir);
        }
        $sql_file = "$backup_dir/$this->id.sql.gz";
        $handle = fopen($sql_file, 'w');

        $this->database->server->decrypt();

        //Set config variables for SSH 
        config(['remote.connections.backup' => [ 'host'      => $this->database->server->address,
									             'username'  => $this->database->server->ssh_username,
									             'password'  => $this->database->server->ssh_password,
									             'timeout'	 => 60 ]]);

        $command = "mysqldump -u " . $this->database->server->username . " -p" . $this->database->server->password ." " . $this->database->name . " | gzip -c" ;

        //SSH into server and run dump command
	 	SSH::into('backup')->run([
	 		$command,
		], function ($line) use (&$handle) {
			fwrite($handle, $line . PHP_EOL);
		});
	 	fclose($handle);
	}
	public function deleteBackupFile() {
		$backup_dir = storage_path('app/public/backups/' . $this->created_at->format('Y-m-d')); 
		$file = "$backup_dir/$this->id.tar.gz";

		if (file_exists($file)) {
			unlink($file);	
		}
	}

	public function restoreTo(Database $database) {

	}

}	
