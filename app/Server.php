<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Crypt;

class Server extends Model {
	protected $hidden = ['password',
						 'ssh_password'];
	protected $guarded = [];

	public function user() {
		return $this->belongsTo('App\User');
	}

	public function databases() {
		return $this->hasMany('App\Database');
	}
	
	public function serverType() {
		return $this->belongsTo('App\ServerType');
	}

	public function decrypt() {
		$this->password = Crypt::decrypt($this->password);
		$this->ssh_password = Crypt::decrypt($this->ssh_password);

		return $this;
	}
}
