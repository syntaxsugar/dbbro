<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function databases() {
        return $this->hasManyThrough('App\Database', 'App\Server');
    }

    public function servers() {
        return $this->hasMany('App\Server');
    }

    public function schedules() {
        return $this->hasMany('App\Schedule');
    }

    public function backups() {
        return $this->hasManyThrough('App\Backup', 'App\Database');
    }
}
