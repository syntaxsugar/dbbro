<?php

namespace App\Console\Commands\Schedule;

use Illuminate\Console\Command;
use App\Schedule;
use DateTime;

class run extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'schedule:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Runs the backup scheduls';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $time = new DateTime();// new DateTime('2017-03-26 17:00:00');
        $schedules = Schedule::at($time)->get();
        $schedules->each(function ($schedule) {
            $schedule->createBackup();
        });
    }
}
