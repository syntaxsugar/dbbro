<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Database;
use App\Jobs\CompressBackupFile;

class BackupTest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'backup:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test Backup of database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $backup_dir = storage_path('app/backups/' . date('Y-m-d')); 

        $sql_file = "$backup_dir/295.sql";

        
        $job = new CompressBackupFile($sql_file);
        $job->handle();
 
    }
}
