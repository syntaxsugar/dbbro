webpackJsonp([16],{

/***/ 41:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(47)(
  /* script */
  __webpack_require__(51),
  /* template */
  __webpack_require__(59),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/Users/gof/Valet/dbbro/resources/assets/js/components/Databases/index.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] index.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-1bda0b62", Component.options)
  } else {
    hotAPI.reload("data-v-1bda0b62", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 47:
/***/ (function(module, exports) {

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = options.computed || (options.computed = {})
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 51:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
	data: function data() {
		return {
			databases: []
		};
	},
	created: function created() {
		// fetch the data when the view is created and the data is
		// already being observed
		this.fetchInitData();
	},

	methods: _defineProperty({
		fetchInitData: function fetchInitData() {
			var _this = this;

			this.$http.get('databases').then(function (response) {
				_this.databases = response.body.databases;
			});
		},
		backupDatabase: function backupDatabase(event) {},
		deleteDatabase: function deleteDatabase(event) {
			var _this2 = this;

			var id = event.target.getAttribute('database-id');
			var index = event.target.getAttribute('index');

			this.$http.post('/databases/' + id, { _method: 'DELETE' }).then(function (response) {
				_this2.databases.splice(index, 1);
			}, function (response) {});
		}
	}, 'backupDatabase', function backupDatabase(event) {
		var id = event.target.getAttribute('database-id');

		this.$http.post('/databases/' + id + '/backup').then(function (response) {
			//add push to backup index
		}, function (response) {});
	})
});

/***/ }),

/***/ 59:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "content-container"
  }, [_c('div', {
    staticClass: "content-header"
  }, [_c('router-link', {
    staticClass: "button",
    attrs: {
      "to": "/databases/create"
    }
  }, [_vm._v("Add Database")])], 1), _vm._v(" "), _c('div', {
    staticClass: "standard-list"
  }, [(_vm.databases.length) ? _c('div', [_vm._m(0), _vm._v(" "), _vm._l((_vm.databases), function(database, index) {
    return _c('div', {
      staticClass: "standard-list-row"
    }, [_c('span', [_c('router-link', {
      attrs: {
        "to": '/databases/' + database.id
      }
    }, [_vm._v(" " + _vm._s(database.name))])], 1), _vm._v(" "), _c('span', [_vm._v(_vm._s(database.description))]), _vm._v(" "), _c('span', [_vm._v(_vm._s(database.server.name) + " (" + _vm._s(database.server.server_type.type) + ")")]), _vm._v(" "), _c('span', [_c('router-link', {
      staticClass: "button button-row",
      attrs: {
        "to": '/databases/' + database.id + '/edit'
      }
    }, [_vm._v("Edit")]), _vm._v(" "), _c('button', {
      staticClass: "button-row",
      attrs: {
        "database-id": database.id,
        "index": index
      },
      on: {
        "click": _vm.backupDatabase
      }
    }, [_vm._v("Backup")]), _vm._v(" "), _c('button', {
      staticClass: "button-row",
      attrs: {
        "database-id": database.id,
        "index": index
      },
      on: {
        "click": _vm.deleteDatabase
      }
    }, [_vm._v("Delete")])], 1)])
  })], 2) : _c('span', {
    staticClass: "empty-state"
  }, [_vm._v("\n\t\t\t\tNo databases have been added yet.\n\t\t\t")])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "standard-list-row standard-list-header"
  }, [_c('span', [_vm._v("Name")]), _vm._v(" "), _c('span', [_vm._v("Description")]), _vm._v(" "), _c('span', [_vm._v("Server")]), _vm._v(" "), _c('span', [_vm._v("Action")])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-1bda0b62", module.exports)
  }
}

/***/ })

});