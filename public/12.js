webpackJsonp([12],{

/***/ 44:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(47)(
  /* script */
  __webpack_require__(54),
  /* template */
  __webpack_require__(65),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/Users/gof/Valet/dbbro/resources/assets/js/components/Servers/edit.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] edit.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-72bf7fb2", Component.options)
  } else {
    hotAPI.reload("data-v-72bf7fb2", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 47:
/***/ (function(module, exports) {

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = options.computed || (options.computed = {})
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 54:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
	data: function data() {
		return {
			errors: {},
			server_types: [],
			ports: [],
			id: this.$route.params.id ? this.$route.params.id : false,
			name: '',
			description: '',
			address: '',
			server_type_id: 0,
			port: 0,
			username: '',
			password: '',
			ssh_username: '',
			ssh_password: ''
		};
	},
	created: function created() {
		// fetch the data when the view is created and the data is
		// already being observed
		this.fetchInitData();
	},

	methods: {
		fetchInitData: function fetchInitData() {
			var _this = this;

			var url = this.id ? 'servers/' + this.id + '/edit' : 'servers/create';
			this.$http.get(url).then(function (response) {
				_this.server_types = response.body.server_types;
				_this.ports = response.body.ports;
				if (response.body.server) {
					var server = response.body.server;
					_this.server_type_id = server.server_type.id;
					_this.name = server.name;
					_this.description = server.description;
					_this.address = server.address;
					_this.port = server.port;
					_this.username = server.username;
					_this.password = server.password;
					_this.ssh_username = server.ssh_username;
					_this.ssh_password = server.ssh_password;
				}
			});
		},
		submit: function submit(event) {
			var _this2 = this;

			var form_data = new FormData(event.target);
			var url = this.id ? 'servers/' + this.id : 'servers';
			if (this.id) {
				form_data.append('id', this.id);
				form_data.append('_method', 'PUT');
			}
			this.$http.post(url, form_data).then(function (response) {
				_this2.$router.push('/servers');
			}, function (response) {
				if (response.status == 422) {
					_this2.errors = response.body;
				}
			});
		},

		hasError: function hasError(field) {
			return this.errors[field] ? this.errors[field] : false;
		}
	},
	watch: {
		server_type_id: function server_type_id() {
			var port = this.ports[this.server_type_id];
			if (port) {
				this.port = port;
			}
		}
	}
});

/***/ }),

/***/ 65:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "content-container"
  }, [_c('h1', [_vm._v(_vm._s((_vm.id) ? 'Update' : 'Create') + " Server")]), _vm._v(" "), _c('form', {
    attrs: {
      "id": "server-form"
    },
    on: {
      "submit": function($event) {
        $event.preventDefault();
        _vm.submit($event)
      }
    }
  }, [_c('select', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.server_type_id),
      expression: "server_type_id"
    }],
    class: {
      'has-error': _vm.hasError('server_type_id')
    },
    attrs: {
      "name": "server_type_id"
    },
    on: {
      "change": function($event) {
        var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
          return o.selected
        }).map(function(o) {
          var val = "_value" in o ? o._value : o.value;
          return val
        });
        _vm.server_type_id = $event.target.multiple ? $$selectedVal : $$selectedVal[0]
      }
    }
  }, [_c('option', {
    attrs: {
      "value": "0",
      "disabled": ""
    }
  }, [_vm._v("Server Type")]), _vm._v(" "), _vm._l((_vm.server_types), function(server_type) {
    return _c('option', {
      domProps: {
        "value": server_type.id
      }
    }, [_vm._v(_vm._s(server_type.type))])
  })], 2), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.name),
      expression: "name"
    }],
    class: {
      'has-error': _vm.hasError('name')
    },
    attrs: {
      "type": "text",
      "placeholder": "Name",
      "name": "name",
      "required": "",
      "autofocus": ""
    },
    domProps: {
      "value": (_vm.name)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.name = $event.target.value
      }
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_c('textarea', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.description),
      expression: "description"
    }],
    class: {
      'has-error': _vm.hasError('description')
    },
    attrs: {
      "type": "text",
      "placeholder": "Description",
      "name": "description"
    },
    domProps: {
      "value": (_vm.description)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.description = $event.target.value
      }
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.address),
      expression: "address"
    }],
    class: {
      'has-error': _vm.hasError('address')
    },
    attrs: {
      "type": "text",
      "placeholder": "Address",
      "name": "address"
    },
    domProps: {
      "value": (_vm.address)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.address = $event.target.value
      }
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.port),
      expression: "port"
    }],
    class: {
      'has-error': _vm.hasError('port')
    },
    attrs: {
      "type": "number",
      "placeholder": "Port",
      "name": "port"
    },
    domProps: {
      "value": (_vm.port)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.port = $event.target.value
      },
      "blur": function($event) {
        _vm.$forceUpdate()
      }
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.username),
      expression: "username"
    }],
    class: {
      'has-error': _vm.hasError('username')
    },
    attrs: {
      "type": "text",
      "placeholder": "Username",
      "name": "username"
    },
    domProps: {
      "value": (_vm.username)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.username = $event.target.value
      }
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.password),
      expression: "password"
    }],
    class: {
      'has-error': _vm.hasError('password')
    },
    attrs: {
      "type": "password",
      "placeholder": "Password",
      "name": "password"
    },
    domProps: {
      "value": (_vm.password)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.password = $event.target.value
      }
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.ssh_username),
      expression: "ssh_username"
    }],
    class: {
      'has-error': _vm.hasError('ssh_username')
    },
    attrs: {
      "ype": "text",
      "placeholder": "SSH Username",
      "name": "ssh_username"
    },
    domProps: {
      "value": (_vm.ssh_username)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.ssh_username = $event.target.value
      }
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.ssh_password),
      expression: "ssh_password"
    }],
    class: {
      'has-error': _vm.hasError('ssh_password')
    },
    attrs: {
      "type": "password",
      "placeholder": "SSH Password",
      "name": "ssh_password"
    },
    domProps: {
      "value": (_vm.ssh_password)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.ssh_password = $event.target.value
      }
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_c('button', {
    attrs: {
      "type": "submit"
    }
  }, [_vm._v("\n    \t\t\t\t" + _vm._s((_vm.id) ? 'Update' : 'Create') + "\n    \t\t\t")]), _vm._v(" "), _c('router-link', {
    staticClass: "button",
    attrs: {
      "to": "/servers"
    }
  }, [_vm._v("Back")])], 1)])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-72bf7fb2", module.exports)
  }
}

/***/ })

});