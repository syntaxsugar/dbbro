webpackJsonp([0],{

/***/ 48:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(49)(
  /* script */
  __webpack_require__(60),
  /* template */
  __webpack_require__(61),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/Users/gof/Valet/dbbro/resources/assets/js/components/Servers/show.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] show.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-01df89b6", Component.options)
  } else {
    hotAPI.reload("data-v-01df89b6", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 49:
/***/ (function(module, exports) {

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = options.computed || (options.computed = {})
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 60:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
	data: function data() {
		return {
			id: this.$route.params.id,
			name: '',
			type: '',
			description: '',
			address: '',
			port: 0,
			username: ''
		};
	},
	created: function created() {
		// fetch the data when the view is created and the data is
		// already being observed
		this.fetchInitData();
	},

	methods: {
		fetchInitData: function fetchInitData() {
			var _this = this;

			this.$http.get('servers/' + this.id).then(function (response) {
				var server = response.body.server;
				_this.name = server.name;
				_this.type = server.server_type.type;
				_this.description = server.description;
				_this.address = server.address;
				_this.port = server.port;
				_this.username = server.username;
				_this.ssh_username = server.ssh_username;
			});
		}
	}
});

/***/ }),

/***/ 61:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "content-container"
  }, [_c('div', {
    staticClass: "standard-show"
  }, [_c('h2', [_vm._v(_vm._s(_vm.name) + " (" + _vm._s(_vm.type) + ")")]), _vm._v(" "), _c('h4', {
    staticClass: "description"
  }, [_vm._v(_vm._s(_vm.description))]), _vm._v(" "), _c('ul', {
    staticClass: "details"
  }, [_c('li', [_c('strong', [_vm._v("Address: ")]), _vm._v(_vm._s(_vm.address) + " (" + _vm._s(_vm.port) + ")")]), _vm._v(" "), _c('li', [_c('strong', [_vm._v("Username: ")]), _vm._v(_vm._s(_vm.username))]), _vm._v(" "), _vm._m(0), _vm._v(" "), _c('li', [_c('strong', [_vm._v("SSH Username: ")]), _vm._v(_vm._s(_vm.ssh_username))]), _vm._v(" "), _vm._m(1)]), _vm._v(" "), _c('router-link', {
    staticClass: "button",
    attrs: {
      "to": "/servers"
    }
  }, [_vm._v("Back")])], 1)])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('li', [_c('strong', [_vm._v("Password: ")]), _vm._v("************")])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('li', [_c('strong', [_vm._v("SSH Password: ")]), _vm._v("************")])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-01df89b6", module.exports)
  }
}

/***/ })

});