webpackJsonp([8],{

/***/ 40:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(49)(
  /* script */
  __webpack_require__(52),
  /* template */
  __webpack_require__(68),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/Users/gof/Valet/dbbro/resources/assets/js/components/Databases/edit.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] edit.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-6a2223aa", Component.options)
  } else {
    hotAPI.reload("data-v-6a2223aa", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 49:
/***/ (function(module, exports) {

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = options.computed || (options.computed = {})
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 52:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
	data: function data() {
		return {
			errors: {},
			servers: {},
			id: this.$route.params.id ? this.$route.params.id : false,
			server_id: 0,
			name: '',
			description: ''

		};
	},
	created: function created() {
		// fetch the data when the view is created and the data is
		// already being observed
		this.fetchInitData();
	},

	methods: {
		fetchInitData: function fetchInitData() {
			var _this = this;

			var url = this.id ? 'databases/' + this.id + '/edit' : 'databases/create';
			this.$http.get(url).then(function (response) {
				_this.servers = response.body.servers;
				if (response.body.database) {
					var database = response.body.database;
					_this.server_id = database.server_id;
					_this.name = database.name;
					_this.description = database.description;
				}
			});
		},
		submit: function submit(event) {
			var _this2 = this;

			var form_data = new FormData(event.target);
			var url = this.id ? 'databases/' + this.id : 'databases';
			if (this.id) {
				form_data.append('id', this.id);
				form_data.append('_method', 'PUT');
			}
			this.$http.post(url, form_data).then(function (response) {
				_this2.$router.push('/databases');
			}, function (response) {
				if (response.status == 422) {
					_this2.errors = response.body;
				}
			});
		},

		hasError: function hasError(field) {
			return this.errors[field] ? this.errors[field] : false;
		}
	}
});

/***/ }),

/***/ 68:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "content-container"
  }, [_c('h1', [_vm._v(_vm._s((_vm.id) ? 'Update' : 'Create') + " Database")]), _vm._v(" "), _c('form', {
    attrs: {
      "id": "database-form"
    },
    on: {
      "submit": function($event) {
        $event.preventDefault();
        _vm.submit($event)
      }
    }
  }, [_c('select', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.server_id),
      expression: "server_id"
    }],
    class: {
      'has-error': _vm.hasError('server_id')
    },
    attrs: {
      "name": "server_id"
    },
    on: {
      "change": function($event) {
        var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
          return o.selected
        }).map(function(o) {
          var val = "_value" in o ? o._value : o.value;
          return val
        });
        _vm.server_id = $event.target.multiple ? $$selectedVal : $$selectedVal[0]
      }
    }
  }, [_c('option', {
    attrs: {
      "value": "0",
      "disabled": ""
    }
  }, [_vm._v("Server")]), _vm._v(" "), _vm._l((_vm.servers), function(server) {
    return _c('option', {
      domProps: {
        "value": server.id
      }
    }, [_vm._v(_vm._s(server.name) + " (" + _vm._s(server.server_type.type) + ")")])
  })], 2), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.name),
      expression: "name"
    }],
    class: {
      'has-error': _vm.hasError('name')
    },
    attrs: {
      "type": "text",
      "placeholder": "Name",
      "name": "name",
      "required": "",
      "autofocus": ""
    },
    domProps: {
      "value": (_vm.name)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.name = $event.target.value
      }
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_c('textarea', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.description),
      expression: "description"
    }],
    attrs: {
      "type": "text",
      "placeholder": "Description",
      "name": "description"
    },
    domProps: {
      "value": (_vm.description)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.description = $event.target.value
      }
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_c('button', {
    attrs: {
      "type": "submit"
    }
  }, [_vm._v("\n    \t\t\t\t" + _vm._s((_vm.id) ? 'Update' : 'Create') + "\n    \t\t\t")]), _vm._v(" "), _c('router-link', {
    staticClass: "button",
    attrs: {
      "to": "/databases"
    }
  }, [_vm._v("Back")])], 1)])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-6a2223aa", module.exports)
  }
}

/***/ })

});