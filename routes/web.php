<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/', 'HomeController@index');
Route::resource('schedules', 'ScheduleController');
Route::resource('servers', 'ServerController');
Route::resource('databases', 'DatabaseController');
Route::post('databases/{id}/backup', 'DatabaseController@backup')->name('databases.backup');
Route::resource('backups', 'BackupController', ['only' => [
	'index', 'show', 'destroy'
]]);


